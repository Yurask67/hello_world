#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
//#include "freertos/queue.h"
#include "timer0_isr.h"
#include "driver/timer.h"
#include "driver/gpio.h"
#include "globalPVPF.h"

//PV defined
//volatile xQueueHandle xQueueTimer1;

//PF declared
static bool timer1_isr_callback();

void timer1_isr_init(void) {
	// инициализация таймера 0, группы 0
	timer_config_t config;
	config.divider = 80; // тактирование счетчика 1 мкс
	config.counter_dir = TIMER_COUNT_UP; // прямой счет
	config.counter_en = TIMER_PAUSE; // счетчик стоит
	config.alarm_en = TIMER_ALARM_EN; // событие перезагрузка разрешено
	config.auto_reload = TIMER_AUTORELOAD_EN; // аппаратная перезагрузка разрешена

	/*Как только таймер включен, его счетчик начинает работать. Чтобы включить таймер,
	 вызовите функцию timer_init () со значением counter_en true или вызовите функцию
	 timer_start (). Начальное значение счетчика таймера можно задать путем вызова
	 timer_set_counter_value (). Для проверки текущего значения таймера вызовите
	 timer_get_counter_value () или timer_get_counter_time_sec ().*/

	//	timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN); //событие перезагрузка разрешено
	timer_init(TIMER_GROUP_1, TIMER_1, &config); // инициализация таймера 0, группы 0 без запуска
	//TODO функции ниже возможны только после инициализации таймера!
	timer_isr_callback_add(TIMER_GROUP_1, TIMER_1, timer1_isr_callback, 0, 0);// callback isr
	timer_set_counter_value(TIMER_GROUP_1, TIMER_1, 0); //считаем с нуля
	timer_set_alarm_value(TIMER_GROUP_1, TIMER_1, 1000000); //1 sec

}

void vTaskTimer1(void *pvParameter) { //функция задачи таймера
	timer1_isr_init();
	timer_start(TIMER_GROUP_1, TIMER_1);

	while (1) {
		vTaskDelay(1000); //?taskYIELD()
	}
}

static bool IRAM_ATTR timer1_isr_callback(void) { // callback isr
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	data.summ++;
	portBASE_TYPE xStatus;
	xStatus = xQueueSendToBackFromISR(xQueueTimer1, &data.summ, 0);
	if (xStatus == pdPASS) {
		xSemaphoreGiveFromISR(xSemaphoreTimer1, &xHigherPriorityTaskWoken);
		if (xHigherPriorityTaskWoken == pdTRUE) //FIXME ХЗЧ
		{
			/* Выдача семафора разблокирует задачу, и приоритет разблокированной
			 задачи выше, чем у текущей выполняющейся задачи - поэтому контекст
			 выполнения переключается принудительно в разблокированную (с более
			 высоким приоритетом) задачу.
			 ВНИМАНИЕ: макрос, реально используемый для переключения контекста
			 из ISR, зависит от конкретного порта FreeRTOS. Здесь указано
			 имя макроса, корректное для порта Open Watcom DOS. Другие порты
			 FreeRTOS могут использовать другой синтаксис. Для определения
			 используемого синтаксиса обратитесь к примерам, предоставленным
			 вместе с портом FreeRTOS. */
			portYIELD_FROM_ISR();
		}
	} else {
		/* Операция отправки не завершена, потому что очередь была заполнена -
		 это должно означать ошибку, так как в нашем случае очередь никогда
		 не будет содержать больше одного элемента данных! */
		// printf( "Could not send to the queue.\r\n" );
	}



	return 1;
}

