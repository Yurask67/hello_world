#include <stdio.h>
#include "Ledc1.h"
#include "driver/ledc.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
//#include "freertos/queue.h"
#include "globalPVPF.h"

ledc_timer_config_t ledc_timer = { .duty_resolution = LEDC_TIMER_13_BIT, // 8191 max for 13 bit PWM duty
		.freq_hz = 5000,                      // frequency of PWM signal
		.speed_mode = LEDC_LS_MODE,           // timer mode
		.timer_num = LEDC_TIMER_0,            // timer index
		.clk_cfg = LEDC_AUTO_CLK,           // Auto select the source clock
		};

ledc_channel_config_t ledc_channel = { .channel = LEDC_CHANNEL_0, //
		.duty = 8191, //
		.gpio_num = 18, //
		.speed_mode = LEDC_LS_MODE, //
		.hpoint = 0, //
		.timer_sel = LEDC_TIMER_0, //
		.intr_type = LEDC_INTR_FADE_END //прерывание по окончанию fade
		};

static void IRAM_ATTR ledc_end_fade(void *arg) { //callback для fade
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xSemaphoreGiveFromISR(xSemaphoreFadeEnd, &xHigherPriorityTaskWoken);
	if (xHigherPriorityTaskWoken == pdTRUE) //FIXME ХЗЧ
	{
		/* Выдача семафора разблокирует задачу, и приоритет разблокированной
		 задачи выше, чем у текущей выполняющейся задачи - поэтому контекст
		 выполнения переключается принудительно в разблокированную (с более
		 высоким приоритетом) задачу.
		 ВНИМАНИЕ: макрос, реально используемый для переключения контекста
		 из ISR, зависит от конкретного порта FreeRTOS. Здесь указано
		 имя макроса, корректное для порта Open Watcom DOS. Другие порты
		 FreeRTOS могут использовать другой синтаксис. Для определения
		 используемого синтаксиса обратитесь к примерам, предоставленным
		 вместе с портом FreeRTOS. */
		portYIELD_FROM_ISR();
	}
}

uint16_t rand_max(uint64_t arg) { //фукция возвращает случайное число из диапазона 0-RAND_MAX

	return (rand() * arg / RAND_MAX);
}

void ledc1(void) {

	ledc_timer_config(&ledc_timer);
	ledc_channel_config(&ledc_channel);
}

void ledc1_cb_add(void) {
	ledc_isr_register(ledc_end_fade, NULL,
	ESP_INTR_FLAG_IRAM | ESP_INTR_FLAG_SHARED, &data.fade_handle); //callback для прерывания по окончанию fade
}

// Task to be created.
void vTaskLedc(void *pvParameters) {
	ledc1(); //инициализация и запуск ШИМ
	ledc_fade_func_install( ESP_INTR_FLAG_IRAM | ESP_INTR_FLAG_SHARED);
	ledc1_cb_add(); //добавляем колбек
	for (;;) {
		// Task code goes here.

		xSemaphoreTake(xSemaphoreFadeEnd, portMAX_DELAY); // попытка взять семафор с бесконечным ожиданием.
		ledc_set_fade_time_and_start(ledc_channel.speed_mode,
				ledc_channel.channel, rand_max(8191), rand_max(1000),
				LEDC_FADE_NO_WAIT);

	}
}

// Function that creates a task.
void vLedc1(void) {
	static uint8_t ucParameterToPass;

	xTaskCreate(vTaskLedc, "Ledc1", 2048, &ucParameterToPass, 5,
			&data.vLedc1_handle);
	configASSERT(data.vLedc1_handle);

}

