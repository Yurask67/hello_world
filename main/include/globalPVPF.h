//#include "nvs_flash.h"
//#include "nvs.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

/*******global PV***********************/
typedef struct var_global {
	uint32_t summ;
	uint8_t fade_end;
	uint64_t fade_handle;
	TaskHandle_t vLedc1_handle;
	TaskHandle_t vTimer1_handle;
	TaskHandle_t vNVS1_handle;
	//	uint16_t vLedc1_handle;
} var_global;

var_global data;

xQueueHandle xQueueTimer1; //handle для очереди TODO нельзя объявлять как static
xSemaphoreHandle xSemaphoreTimer1; //handle для семафора - помещение элемента в очередь в прерывании таймера прерывания таймера
xSemaphoreHandle xSemaphoreFadeEnd; //handle для семафора - окончание работы fade


/******global PF***********************/
void vTaskTimer1(void *pvParameter);
void vTaskNVS1(void *pvParameters);
void vEncoder(void);
//int rand_max(int *arg);
