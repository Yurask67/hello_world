#include <stdio.h>
#include "Nvs1.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "globalPVPF.h"

void vTaskNVS1(void *pvParameters) {
	int32_t counter1 = 0; //������� 1
	int32_t counter2 = 0; //������� 2

	// �������������  NVS
	esp_err_t err = nvs_flash_init();
	if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		// ���� NVS ������ ��� ������ ��� ������� ��������
		// TODO ����� ����������� 3 ������
		ESP_ERROR_CHECK(nvs_flash_erase());
		err = nvs_flash_init();
	}
	ESP_ERROR_CHECK(err);

	// Open..................................
	printf("\n");
	printf("Opening Storage handle NVS1 & NVS2 ");

	nvs_handle_t NVS1_handle;		//��������� ������ MVS1
	err = nvs_open("NVS1", NVS_READWRITE, &NVS1_handle);
	if (err != ESP_OK) {
		printf("Error (%s) opening NVS1 handle!\n", esp_err_to_name(err));
	} else {
		printf("Done\n");

		nvs_handle_t NVS2_handle;		//��������� ������ MVS2
		err = nvs_open("NVS2", NVS_READWRITE, &NVS2_handle);
		if (err != ESP_OK) {
			printf("Error (%s) opening NVS2 handle!\n", esp_err_to_name(err));
		} else {
			printf("Done\n");

			// ������............................
			// ����1...............
			printf("Reading counter1 from NVS1 ... ");
			err = nvs_get_i32(NVS1_handle, "counter1", &counter1); //2� �������� - ����
			switch (err) {
			case ESP_OK:
				printf("Done\n");
				printf("Finded counter1 = %d\n", counter1);
				break;
			case ESP_ERR_NVS_NOT_FOUND: // ���� ���� (������) �� ������
				printf("The value is not initialized yet!\n");
				printf("Initialized value cuanter1...\n");
				err = nvs_set_i32(NVS1_handle, "counter1", counter1); //���������� ������ ��� 0
				printf("Committing updates in NVS1 ... ");
				err = nvs_commit(NVS1_handle);
				printf((err != ESP_OK) ? "Failed!\n" : "Done\n");

				break;
			default:
				printf("Error (%s) reading!\n", esp_err_to_name(err));
				printf((err != ESP_OK) ? "Failed!\n" : "Done\n");
			}

			//����2.................
			printf("Reading counter2 from NVS2 ... ");
			err = nvs_get_i32(NVS2_handle, "counter2", &counter2); //2� �������� - ����
			switch (err) {
			case ESP_OK:
				printf("Done\n");
				printf("Finded counter2 = %d\n", counter2);
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				printf("The value is not initialized yet!\n");
				printf("Initialized value cuanter2...\n");
				err = nvs_set_i32(NVS2_handle, "counter2", counter2); //���������� ������ ��� 0
				printf("Committing updates in NVS2 ... ");
				err = nvs_commit(NVS2_handle);
				printf((err != ESP_OK) ? "Failed!\n" : "Done\n");

				break;
			default:
				printf("Error (%s) reading!\n", esp_err_to_name(err));
				printf((err != ESP_OK) ? "Failed!\n" : "Done\n");
			}
		}

		while (1) {
			for (int var = 0; var < 10; ++var) {

				xSemaphoreTake(xSemaphoreFadeEnd, portMAX_DELAY); // ������� ����� ������� � ����������� ���������.
				counter1++;
				counter2++;
				counter2++;
				vTaskDelay(1000 / portTICK_RATE_MS);
			}
			//������ ����1.............
			printf("Write new value cuanter1= %d\n", counter1);
			err = nvs_set_i32(NVS1_handle, "counter1", counter1); //���������� ����� �������� ����� 10 �����
			printf("Committing updates in NVS1 ... ");
			err = nvs_commit(NVS1_handle);
			printf((err != ESP_OK) ? "Failed!\n" : "Done\n");

			//������ ����2.............
			printf("Write new value cuanter2= %d\n", counter2);
			err = nvs_set_i32(NVS2_handle, "counter2", counter2); //���������� ����� �������� ����� 10 �����
			printf("Committing updates in NVS2 ... ");
			err = nvs_commit(NVS2_handle);
			printf((err != ESP_OK) ? "Failed!\n" : "Done\n");
		}
	}
}
//		// Write
//		printf("Updating restart counter in NVS ... ");
//		restart_counter++;
//		err = nvs_set_i32(my_handle, "restart_counter", restart_counter);
//		printf((err != ESP_OK) ? "Failed!\n" : "Done\n");
//
//		// Commit written value.
//		// After setting any values, nvs_commit() must be called to ensure changes are written
//		// to flash storage. Implementations may write to storage at other times,
//		// but this is not guaranteed.
//		printf("Committing updates in NVS ... ");
//		err = nvs_commit(NVS1_handle);
//		printf((err != ESP_OK) ? "Failed!\n" : "Done\n");
//
//		// Close
//		nvs_close(my_handle);
//	}
//
//	printf("\n");
//
//	// Restart module
//	for (int i = 10; i >= 0; i--) {
//		printf("Restarting in %d seconds...\n", i);
//		vTaskDelay(1000 / portTICK_PERIOD_MS);
//	}
//	printf("Restarting now.\n");
//	fflush(stdout);
//	esp_restart();
//}

