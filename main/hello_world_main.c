/* Hello World Example
 This example code is in the Public Domain (or CC0 licensed, at your option.)
 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "Ledc1.h"
#include "Timer0_isr.h"
#include "Nvs1.h"
#include "globalPVPF.h"
#include "nvs_flash.h"

void hello_task(void *pvParameter) {
	uint32_t ReceivedValue;
	portBASE_TYPE xStatus;

	/* Эта задача также определена как бесконечный цикл. */
	for (;;) {
		xSemaphoreTake(xSemaphoreTimer1, portMAX_DELAY); /*берем семафор что было прерывание,
		 если семафора нет ожидаем его появления бесконечно долго*/
		if (uxQueueMessagesWaiting(xQueueTimer1) != 0) {
//			printf("uxQueueMessagesWaiting=%d\r\n",uxQueueMessagesWaiting(xQueueTimer1));
			// Прием данных из очереди.
			xStatus = xQueueReceive(xQueueTimer1, &ReceivedValue, 0);
			if (xStatus == pdPASS) {
				/* Данные успешно приняты из очереди, печать принятого значения. */
//				printf("Received = %d\n", ReceivedValue);
			} else {
				/* Данные не были приняты из очереди даже после ожидания 100 мс.
				 Это должно означать ошибку, так как отправляющие данные в очередь
				 задачи работают постоянно записывают данные в очередь. */
//				printf("Could not receive from the queue.\r\n");
			}
		}
		taskYIELD();
	}
}

void app_main() {
	//nvs_flash_init();
	/* XXX перед созданием задач создаем очереди, семафоры и прочее здесь в первую очередь. Если они
	 *  используются только	внутри одной задачи - можно создавать их в самой задаче, иначе могут быть
	 *  проблемы что задача	обращается к семафору, а он еще не создан*/
	xQueueTimer1 = xQueueCreate(5, sizeof(data.summ)); //создание очереди для таймера
	vSemaphoreCreateBinary(xSemaphoreTimer1); //создаем бинарный семафор для таймера
	vSemaphoreCreateBinary(xSemaphoreFadeEnd); //создаем бинарный семафор для LEDC fade - возвращается в callback-е fade

	xTaskCreate(&hello_task, "hello_task", 2048, NULL, 5, NULL);
	vLedc1(); //start task Ledc
	xTaskCreate(&vTaskTimer1, "timer_task", 2048, NULL, 5,
			&data.vTimer1_handle);
	//xTaskCreate(&vTaskNVS1, "nvs_task", 2048, NULL, 5, &data.vNVS1_handle);
	vEncoder();

}
